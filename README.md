# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://106000125.gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/04 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
<img src="demo.png" width="700px"></img>
* **Basic components:**
1. Basic control tools: Done
2. Text input : Done, user shouldn't input at other places before the previous input text has been entered.
   Size of brush and text are selected through the same range input item.
3. Cursor icon: Done
4. Refresh button: Done

* **Advance tools:**
1. Different brush shapes: Done
2. Un/Re-do button: Done
3. Image tool: Done, just simply upload the image and it'll be pasted on the canvas. 
               Notice that it won't upload when the user is choosing the same image.
4. Download: Done, downloaded as a .png file

* **Appearance:**
1. It looks nice.
2. Button will change it's background color while being hovered by mouse.
3. Different image for Filling tool to see the filling state.

* **Other widgets:**
1. Line: Draw straight line
2. Rainbow: Brush that is filled with gradient color
3. Filling: For the shape tools, choose to fill it or not
