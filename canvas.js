if(window.addEventListener) {
  window.addEventListener('load', function () {
    var canvas, context, canvaso, contexto;
    var gradient
    var cArray = new Array();
    var cStep = -1;
    var tool = false;
    var tool_default = 'brush';

    // Initialization sequence.
    function init () {
      // Find the canvas element.
      

      canvaso = document.getElementById('myCanvas');
      if (!canvaso) {
        alert('Error: I cannot find the canvas element!');
        return;
      }
  
      if (!canvaso.getContext) {
        alert('Error: no canvas.getContext!');
        return;
      }
  
      // Get the 2D canvas context.
      contexto = canvaso.getContext('2d');
      if (!contexto) {
        alert('Error: failed to getContext!');
        return;
      }

      // Add the temporary canvas
      var container = canvaso.parentNode;
      canvas = document.createElement('canvas');
      if (!canvas) {
        alert('Error: I cannot create a new <canvas> element!');
        return;
      }
      canvas.id = 'imageTemp';
      canvas.width = canvaso.width;
      canvas.height = canvaso.height;
      container.appendChild(canvas);
      context = canvas.getContext('2d');
      contexto.fillStyle = '#ffffff';
      contexto.fillRect( 0, 0, canvas.width, canvas.height ); 
      cPush();
      document.getElementById('reset').addEventListener('click',reset,false);
      var tool_brush = document.getElementById('s_brush');
      var tool_eraser = document.getElementById('s_eraser');
      var upload = document.getElementById('upload');
      var tool_rect = document.getElementById('s_rect');
      var tool_line = document.getElementById('s_line');
      var tool_tria = document.getElementById('s_tria');
      var tool_circle = document.getElementById('s_circle');
      var tool_text = document.getElementById('s_text');
      var tool_redo = document.getElementById('s_redo');
      var tool_undo = document.getElementById('s_undo');
      var tool_save = document.getElementById('s_save');
      var tool_fill = document.getElementById('s_fill');
      var tool_rainbow = document.getElementById('s_rainbow');
      
      tool_brush.addEventListener('click', ev_tool_change, false);
      tool_rect.addEventListener('click', ev_tool_change, false);
      tool_tria.addEventListener('click', ev_tool_change, false);
      tool_circle.addEventListener('click', ev_tool_change, false);
      tool_eraser.addEventListener('click', ev_tool_change, false);
      tool_text.addEventListener('click', ev_tool_change, false);
      tool_line.addEventListener('click', ev_tool_change, false);
      tool_fill.addEventListener('click', fill, false);
      upload.addEventListener('click', upLoad, false);
      tool_save.addEventListener('click', save, false);
      tool_undo.addEventListener('click', cUndo, false);
      tool_redo.addEventListener('click', cRedo, false);
      tool_rainbow.addEventListener('click', ev_tool_change, false);
      
      if (tools[tool_default]) {
        tool = new tools[tool_default]();
      }
      var color = document.getElementById('favcolor');
      var radius = document.getElementById('font_size');
      document.getElementById("range_val").innerHTML=radius.value;
      color.addEventListener('change',color_change, false);
      radius.addEventListener('input',radius_change, false);
      gradient = context.createLinearGradient(0, 0, 0, 500);
      gradient.addColorStop("0", "#EB8E98");
      gradient.addColorStop("0.2" ,"#FBC9CC");
      gradient.addColorStop("0.5", "#FEE2D7");
      gradient.addColorStop("0.8", "#93D9D8");
      gradient.addColorStop("1", "#11454F");
      context.strokeStyle = color.value;
      context.lineWidth =radius.value;
      context.lineCap = "round";
      context.lineJoin="round";
      context.textBaseline = 'top';
      // Attach the mousemove event handler.
      canvas.addEventListener('mousedown', ev_canvas, false);
      canvas.addEventListener('mouseup', ev_canvas, false);
      canvas.addEventListener('mousemove', ev_canvas, false);
      document.body.style.cursor = 'url("cursor/s_brush.png"),pointer';
    }
    
    var fill_f = true;
    function fill(){
      fill_f = !fill_f;
      console.log('fill_f: '+fill_f);
      if(fill_f){
        document.getElementById("s_fill").src = "icon/fill.png";
      }
      else{
        document.getElementById("s_fill").src = "icon/circle.png";
      }
    }

    function reset(){
      console.log('reset');
      contexto.clearRect(0, 0, canvas.width, canvas.height);
      contexto.fillRect( 0, 0, canvas.width, canvas.height );  
      cPush();
    }

    var rainbow_f = false;

    function rainbow(){
      ellipse(mouseX, mouseY, 50,50);
      rainbow_f = !rainbow_f;
      var tool_rainbow = document.getElementById('s_rainbow');
     console.log(rainbow_f);
      if(rainbow_f){
        tool_rainbow.style.background = '#FFCCA5';
        color_change();
      }
      else{
        tool_rainbow.style.background = '#FFE8D8';
        color_change();
      }
      
    }

    
    
    function color_change(){
      console.log('color_change');
      var color = document.getElementById('favcolor');
      context.strokeStyle = color.value;
      context.fillStyle = color.value;
        
    };
     
    
    function eraser(){
      console.log('eraser');
      context.strokeStyle = '#ffffff';
    }
    function radius_change(){
      context.lineWidth = this.value;
      document.getElementById("range_val").innerHTML=this.value;
    }

    function ev_tool_change (ev) {
      document.body.style.cursor = 'url("cursor/'+this.id+'.png"),pointer';
      context.strokeStyle = document.getElementById('favcolor').value;
      context.fillStyle = document.getElementById('favcolor').value;
      console.log(this.value);
      if (tools[this.value]) {
        if(this.id=="s_eraser") eraser();
        tool = new tools[this.value]();
      }
    }
    function img_update () {
      console.log("img update");
      contexto.drawImage(canvas, 0, 0);
      cPush();
      context.clearRect(0, 0, canvas.width, canvas.height);
    }
    var tools = {};
    var graColor = ["#C974F2","#F2605C", "#D97C43","#F0DC4D", "#8689F2"];
    function upLoad(){
      document.getElementById("upload").oninput = function(){
        var img = new Image();
        img.onload = function(){
          contexto.drawImage(this, 0, 0, canvaso.width, canvaso.height);
          cPush();
        }
        img.src = URL.createObjectURL(this.files[0]);
        console.log(img.src);
      };
      img_update();
      
    }

    function cPush(){
      cStep++;
      if(cStep < cArray.length){
        cArray.length = cStep;
      }
      cArray.push(canvaso.toDataURL());
    }

    function cUndo(){
      console.log(cStep + " " + cArray.length);
      if(cStep > 0){
        cStep--;
        var cPic = new Image();
        cPic.src = cArray[cStep];
        cPic.onload = function(){
          contexto.drawImage(cPic, 0, 0);
        }
      }
    }

    function cRedo(){
      if(cStep < cArray.length -1){
        cStep++;
        var cPic = new Image();
        cPic.src = cArray[cStep];
        cPic.onload = function (){
          contexto.drawImage(cPic, 0, 0);
        }
      }
    }

    

    tools.line = function () {
      var tool = this;
      this.started = false;
    
      this.mousedown = function (ev) {
        tool.started = true;
        tool.x0 = ev._x;
        tool.y0 = ev._y;
      };
    
      this.mousemove = function (ev) {
        if (!tool.started) {
          return;
        }
    
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.beginPath();
        context.moveTo(tool.x0, tool.y0);
        context.lineTo(ev._x, ev._y);
        context.stroke();
        context.closePath();
      };
    
      this.mouseup = function (ev) {
        if (tool.started) {
          tool.mousemove(ev);
          tool.started = false;
          img_update();
        }
      };
    };
    tools.brush = function(){
      var tool = this;
      this.started = false;
      this.mousedown = function(ev){
        context.beginPath();
        context.moveTo(ev._x, ev._y);
        tool.started = true;
      }

      this.mousemove = function(ev){
        if(tool.started){
          context.lineTo(ev._x, ev._y);
          context.stroke();
        }
      }

      this.mouseup = function(ev){
        if(tool.started){
          tool.mousemove(ev);
          tool.started = false;
          img_update ();
        }
      }
    }
    tools.rainbow = function(){
      var tool = this;
      this.started = false;
      this.mousedown = function(ev){
        context.beginPath();
        context.moveTo(ev._x, ev._y);
        tool.started = true;
      }

      this.mousemove = function(ev){
        if(tool.started){
          context.lineTo(ev._x, ev._y);
          context.strokeStyle = gradient;
          context.stroke();
        }
      }

      this.mouseup = function(ev){
        if(tool.started){
          tool.mousemove(ev);
          tool.started = false;
          img_update ();
        }
      }
    }

    tools.circle = function () {
      var tool = this;
      this.started = false;

      this.mousedown = function (ev) {
        
        tool.started = true;
        tool.x0 = ev._x;
        tool.y0 = ev._y;
      };
    
      this.mousemove = function (ev) {
        if (!tool.started) {
          return;
        }
        context.beginPath();
        var rx = Math.abs(ev._x - tool.x0),
        ry = Math.abs(ev._y - tool.y0),
        r= Math.max(rx,ry);
        
        console.log('clear');
        context.clearRect(0, 0, canvas.width, canvas.height);
        if (!rx || !ry) {
          return;
        }
        context.arc(tool.x0, tool.y0, r, 0, 2 * Math.PI,false);
        context.closePath();
        if(!fill_f)context.stroke();
        else context.fill();
      };
    
      this.mouseup = function (ev) {
        if (tool.started) {
          tool.mousemove(ev);
          tool.started = false;
          img_update ();
        }
      };
    };

    tools.tria = function () {
      var tool = this;
      this.started = false;

      this.mousedown = function (ev) {
        
        tool.started = true;
        tool.x0 = ev._x;
        tool.y0 = ev._y;
      };
    
      this.mousemove = function (ev) {
        if (!tool.started) {
          return;
        }
        context.beginPath();
        var top_x = (ev._x + tool.x0)/2;
        
        console.log('clear');
        context.clearRect(0, 0, canvas.width, canvas.height);
   
        moveTo(tool.x0, tool.y0);
        context.lineTo(ev._x, tool.y0);
        context.lineTo(top_x, ev._y);
        context.lineTo(tool.x0, tool.y0);
        context.closePath();
        if(!fill_f)context.stroke();
        else context.fill();
        
      };
    
      this.mouseup = function (ev) {
        if (tool.started) {
          tool.mousemove(ev);
          tool.started = false;
          img_update ();
        }
      };
    };

    tools.text = function(){
      var tool = this;
      this.started = false;
      var all_str = "";
      var flag = 0;
      this.mousedown = function (ev) {
        tool.started = true;
        if(flag ==0){
          tool.x0 = ev._x;
          tool.y0 = ev._y;
        }
        var input = document.createElement('input');
        if (!input) {
          alert('Error: I cannot create a new <input> element!');
          return;
        }
        input.type = 'text';
        input.id = "textTemp";
        input.style.position = 'absolute';
        input.style.left = tool.x0 + 'px';
        input.style.top = tool.y0 + 'px';
        input.style.fontFamily = document.getElementById("font_style").value;
        input.style.fontSize = document.getElementById("font_size").value+ 'px';
        console.log(input.style.fontSize);
        if(flag==0){
          container.appendChild(input);
        }
        flag = 1;
        input.onkeydown = function(ev){
          var evKeycode = ev.keyCode;
          if(evKeycode ==13) {//Enter
            context.font = input.style.fontSize +' '+input.style.fontFamily;
            context.fillText(this.value, tool.x0, tool.y0);
            tool.started = false;
            flag =0;
            container.removeChild(input);
            img_update();
          }
          else{
            console.log(this.value);
          }
        }
      };

    }

    tools.rect = function () {
      var tool = this;
      this.started = false;
    
      this.mousedown = function (ev) {
        tool.started = true;
        tool.x0 = ev._x;
        tool.y0 = ev._y;
      };
    
      this.mousemove = function (ev) {
        if (!tool.started) {
          return;
        }
    
        var x = Math.min(ev._x,	tool.x0),
          y = Math.min(ev._y,	tool.y0),
          w = Math.abs(ev._x - tool.x0),
          h = Math.abs(ev._y - tool.y0);
    
        context.clearRect(0, 0, canvas.width, canvas.height);
        console.log('clear');
        if (!w || !h) {
          return;
        }
        if(!fill_f)context.strokeRect(x, y, w, h);
        else context.fillRect(x, y, w, h);
      };
    
      this.mouseup = function (ev) {
        if (tool.started) {
          tool.mousemove(ev);
          tool.started = false;
          img_update ();
        }
      };
    };
    
    function save (){
      var image = canvaso.toDataURL();        // create temporary link      
      var tmpLink = document.createElement( 'a' );      
      tmpLink.download = 'image.png'; // set the name of the download file     
      tmpLink.href = image;        // temporarily add link to body and initiate the download      
      document.body.appendChild( tmpLink );      
      tmpLink.click();      
      document.body.removeChild( tmpLink );
    }
    // The mousemove event handler.
    function ev_canvas (ev) {
      // Get the mouse position relative to the canvas element.
      if (ev.layerX || ev.layerX == 0) { // Firefox
        ev._x = ev.layerX;
        ev._y = ev.layerY;
      } else if (ev.offsetX || ev.offsetX == 0) { // Opera
        ev._x = ev.offsetX;
        ev._y = ev.offsetY;
      }
      
      // Call the event handler of the tool
      var func = tool[ev.type];
      if (func) {
        func(ev);
      }
    }
  
    init();
  }, false); 
}


